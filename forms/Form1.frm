VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   7230
   ClientLeft      =   3120
   ClientTop       =   1740
   ClientWidth     =   9030
   LinkTopic       =   "Form1"
   ScaleHeight     =   7230
   ScaleWidth      =   9030
   Begin VB.CommandButton Command4 
      Caption         =   "Fix Alarm Counts"
      Height          =   615
      Left            =   5520
      TabIndex        =   8
      Top             =   720
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Destroy Dupes"
      Height          =   615
      Left            =   3120
      TabIndex        =   6
      Top             =   2400
      Width           =   2175
   End
   Begin MSComctlLib.ProgressBar ProgressBar2 
      Height          =   255
      Left            =   1800
      TabIndex        =   5
      Top             =   5280
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Import Alarm Company"
      Height          =   615
      Left            =   3120
      TabIndex        =   3
      Top             =   1560
      Width           =   2175
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   375
      Left            =   1800
      TabIndex        =   1
      Top             =   4080
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Import Addresses"
      Height          =   615
      Left            =   3120
      TabIndex        =   0
      Top             =   720
      Width           =   2175
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   255
      Left            =   1680
      TabIndex        =   7
      Top             =   5760
      Width           =   3855
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   255
      Left            =   1680
      TabIndex        =   4
      Top             =   4800
      Width           =   4695
   End
   Begin VB.Label Label1 
      Caption         =   "Importing Record"
      Height          =   255
      Left            =   1680
      TabIndex        =   2
      Top             =   3600
      Width           =   6735
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_lngAcctID As Long

Public Function CountAccounts() As Integer

   'This function see's how many alarms there are for an account
   Dim CollierConnectionStr As String
   Dim CollierConnection As New ADODB.Connection
   Dim CollierCommand As New ADODB.Command
   Dim CollierRecordSet As New ADODB.Recordset
   
   CollierConnectionStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\faap.mdb;Mode=ReadWrite|Share Deny None;Persist Security Info=False"
   CollierConnection.ConnectionString = CollierConnectionStr
   CollierConnection.CursorLocation = adUseClient
   CollierConnection.Open
   
   With CollierCommand
       .ActiveConnection = CollierConnection
       .CommandText = "SELECT * FROM FAB_Address;"
       .CommandType = adCmdText
   End With
   
   With CollierRecordSet
       .CursorType = adOpenStatic
       .CursorLocation = adUseClient
       .LockType = adLockOptimistic
       .Open CollierCommand
   End With
   
   CountAccounts = CollierRecordSet.RecordCount
   CollierRecordSet.Close

End Function

Public Function CountAlarm(Account_ID As Integer) As Integer

   'This function see's how many alarms there are for an account
   
   Dim CollierConnectionStr As String
   Dim CollierConnection As New ADODB.Connection
   Dim CollierCommand As New ADODB.Command
   Dim CollierRecordSet As New ADODB.Recordset
   
   CollierConnectionStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\faap.mdb;Mode=ReadWrite|Share Deny None;Persist Security Info=False"
   CollierConnection.ConnectionString = CollierConnectionStr
   CollierConnection.CursorLocation = adUseClient
   CollierConnection.Open
   
   With CollierCommand
       .ActiveConnection = CollierConnection
       .CommandText = "SELECT * FROM Alarm WHERE Account=" & Account_ID & ";"
       .CommandType = adCmdText
   End With
   
   With CollierRecordSet
       .CursorType = adOpenStatic
       .CursorLocation = adUseClient
       .LockType = adLockOptimistic
       .Open CollierCommand
   End With
   
   CountAlarm = CollierRecordSet.RecordCount
   CollierRecordSet.Close
   
End Function

' import address

Private Sub Command1_Click()

   Dim CollierConnectionStr As String
   Dim FABConnectionStr As String
   Dim CollierConnection As New ADODB.Connection
   Dim CollierCommand As New ADODB.Command
   Dim rsFAAP_Account As New ADODB.Recordset
   Dim CollierConnection2 As New ADODB.Connection
   Dim CollierCommand2 As New ADODB.Command
   Dim rsFAAP_Account_Contact As New ADODB.Recordset
   Dim RegConnection As New ADODB.Connection
   Dim RegCommand As New ADODB.Command
   Dim rsReg As New ADODB.Recordset
   Dim FABConnection As New ADODB.Connection
   Dim FABCommand As New ADODB.Command
   Dim FABRecordSet As New ADODB.Recordset
   Dim counter As Integer
   Dim StreetSplit() As String
   Dim strSortStreet() As String
   Dim tempsql As String
   Dim strAddressee As String
   Dim alarmcount As Long
   Dim lastwait
   Dim Other_Contact As String
   Dim registration_initial, registration_last, responsible_1_day_phone, property_type
   Dim Billing_Street, Billing_City, Billing_State, Billing_Zip, Billing_Phone
   
   counter = 0
   
   'All the database opening stuff
   CollierConnectionStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\faap.mdb;Mode=ReadWrite|Share Deny None;Persist Security Info=False"
   FABConnectionStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\False_Alarm_Billing.mdb;Mode=ReadWrite|Share Deny None;Persist Security Info=False"
   
   CollierConnection.ConnectionString = CollierConnectionStr
   CollierConnection2.ConnectionString = CollierConnectionStr
   FABConnection.ConnectionString = FABConnectionStr
   RegConnection.ConnectionString = FABConnectionStr
   
   Set DB_CONNECTION = FABConnection

   CollierConnection.CursorLocation = adUseClient
   CollierConnection2.CursorLocation = adUseClient
   FABConnection.CursorLocation = adUseClient
   
   RegConnection.CursorLocation = adUseClient
         
   CollierConnection.Open
   CollierConnection2.Open
   FABConnection.Open
   RegConnection.Open
   
   'Set the data I want to pull and the table ill be using.
   With CollierCommand
      .ActiveConnection = CollierConnection
      .CommandText = "SELECT * FROM Account;"
      .CommandType = adCmdText
   End With
      
   With FABCommand
      .ActiveConnection = FABConnection
      .CommandText = "SELECT * FROM FAB_Address;"
      .CommandType = adCmdText
   End With
   
   With RegCommand
      .ActiveConnection = FABConnection
      .CommandText = "SELECT * FROM FAB_Registration;"
      .CommandType = adCmdText
   End With

   'Open my SQL queries
   Label1.Caption = "Opening Account Table Query"
   Me.Refresh
   With rsFAAP_Account
      .CursorType = adOpenForwardOnly
      .CursorLocation = adUseClient
      .LockType = adLockReadOnly
      .Open CollierCommand
   End With
   Label1.Caption = "Opening FAB Query"
   Me.Refresh
   With FABRecordSet
      .CursorType = adOpenForwardOnly
      .CursorLocation = adUseClient
      .LockType = adLockReadOnly
      '.Open FABCommand
   End With
   
   With rsReg
      .CursorType = adOpenForwardOnly
      .CursorLocation = adUseClient
      .LockType = adLockReadOnly
      '.Open RegCommand
   End With
   
   CollierCommand2.ActiveConnection = CollierConnection2
   CollierCommand2.CommandType = adCmdText
   With rsFAAP_Account_Contact
      .CursorType = adOpenStatic
      .CursorLocation = adUseClient
      .LockType = adLockOptimistic
   End With
   
   'Start at first record,  go through all of them also pulling the Primary contact info from a seprate DB
   lastwait = 0
   
   rsFAAP_Account.MoveFirst
   ProgressBar1.Max = rsFAAP_Account.RecordCount
   
   Do While (Not rsFAAP_Account.EOF)
      Billing_Street = ""
      Billing_City = ""
      Billing_State = ""
      Billing_Zip = ""
      Billing_Phone = ""
      counter = counter + 1
      ProgressBar1.Value = counter
      DoEvents

      If Trim$(rsFAAP_Account.Fields("First_Name").Value) <> "" Then
         strAddressee = Trim$(rsFAAP_Account.Fields("First_Name").Value) + " " + Trim$(rsFAAP_Account.Fields("Name").Value)
      Else
         strAddressee = Trim$(rsFAAP_Account.Fields("Name").Value)
      End If
      
    'If (counter = 500 Or counter = (lastwait + 500)) Then
        'lastwait = counter
        'Wait 2
   ' End If

      Label1.Caption = "Importing Record " & counter & " of " & rsFAAP_Account.RecordCount
      Me.Refresh
        
      'Check for Other Contact (Keyholder)
      tempsql = "SELECT * FROM Account_Contact WHERE ID =" & rsFAAP_Account.Fields(4) '  & " AND ID <>" & rsFAAP_Account.Fields("Primary_Contact") & " AND ID <> " & rsFAAP_Account.Fields("Billing_Contact") & ";"
      CollierCommand2.CommandText = tempsql
      If counter = 1 Then
         rsFAAP_Account_Contact.Open CollierCommand2
      Else
         rsFAAP_Account_Contact.Requery
      End If
      
      If Not rsFAAP_Account_Contact.EOF Then
         Other_Contact = Clean_String(rsFAAP_Account_Contact.Fields("Contact").Value)
      Else
         Other_Contact = ""
      End If
    
      m_lngAcctID = rsFAAP_Account.Fields("ID").Value
      
     'Find the Matching Primary Contact
     If rsFAAP_Account.Fields("Same_As_Primary") = True Then
        tempsql = "SELECT * FROM Account_Contact WHERE ID =" & rsFAAP_Account.Fields("Primary_Contact").Value & ";"
        CollierCommand2.CommandText = tempsql
        rsFAAP_Account_Contact.Requery

        Billing_Street = Clean_String(rsFAAP_Account_Contact.Fields("Address1"))
        Billing_City = Clean_String(rsFAAP_Account_Contact.Fields("City"))
        Billing_State = Clean_String(rsFAAP_Account_Contact.Fields("Region_Code"))
        Billing_Zip = Clean_String(rsFAAP_Account_Contact.Fields("Postal_Code"))
        Billing_Phone = Clean_String(rsFAAP_Account_Contact.Fields("Phone_Number"))
     Else
        'Get the billing info,  then we also need the primary contact info
        tempsql = "SELECT * FROM Account_Contact WHERE ID =" & rsFAAP_Account.Fields("Billing_Contact") & ";"
        CollierCommand2.CommandText = tempsql
        rsFAAP_Account_Contact.Requery
    
        Billing_Street = Clean_String(rsFAAP_Account_Contact.Fields("Address1"))
        Billing_City = Clean_String(rsFAAP_Account_Contact.Fields("City"))
        Billing_State = Clean_String(rsFAAP_Account_Contact.Fields("Region_Code"))
        Billing_Zip = Clean_String(rsFAAP_Account_Contact.Fields("Postal_Code"))
        Billing_Phone = Clean_String(rsFAAP_Account_Contact.Fields("Phone_Number"))
    
        tempsql = "SELECT * FROM Account_Contact WHERE ID =" & rsFAAP_Account.Fields("Primary_Contact") & ";"
        CollierCommand2.CommandText = tempsql
        rsFAAP_Account_Contact.Requery
      End If
      'rsFAAP_Account_Contact.Open CollierCommand2
    
      'rsReg.AddNew
      If Billing_Phone <> "" Then
         responsible_1_day_phone = Billing_Phone
      Else
         responsible_1_day_phone = rsFAAP_Account_Contact.Fields("Phone_Number")
      End If
      
      If rsFAAP_Account.Fields("Registered") = True Or rsFAAP_Account.Fields("Registered") = "yes" Then
         'rsReg.Fields("registration_initial").Value = rsFAAP_Account.Fields("Create_Date").Value
         registration_initial = rsFAAP_Account.Fields("Create_Date").Value

         RegCommand.CommandText = "INSERT INTO FAB_Registration (" & _
            "[registration_initial], " & _
            "[registration_last], " & _
            "[registration_expire], " & _
            "[account_id], " & _
            "[responsible_1_day_phone], " & _
            "[responsible_1_name], " & _
            "[contact_1], " & _
            "[Monitoring_Alarm_Company], " & _
            "[Servicing_Alarm_Company], " & _
            "[permit_number]) VALUES (" & _
            "#" & registration_initial & "#, " & _
            "#" & rsFAAP_Account.Fields("Create_Date").Value & "#, " & _
            "#" & rsFAAP_Account.Fields("Permit_Due").Value & "#, " & _
            m_lngAcctID & ", " + _
            "'" & responsible_1_day_phone & "', " + _
            "'" & Other_Contact & "', " & _
            "'" + Clean_String(rsFAAP_Account_Contact.Fields("Contact")) & "', " & _
            rsFAAP_Account.Fields("Monitoring_Alarm_Company").Value & ", " & _
            rsFAAP_Account.Fields("Servicing_Alarm_Company").Value & ", " & _
            "'" & rsFAAP_Account.Fields("Number").Value & "');"
      Else
         RegCommand.CommandText = "INSERT INTO FAB_Registration (" + _
            "[registration_last], " & _
            "[registration_expire], " & _
            "[account_id], " & _
            "[responsible_1_day_phone], " & _
            "[responsible_1_name], " & _
            "[contact_1], " & _
            "[Monitoring_Alarm_Company], " & _
            "[Servicing_Alarm_Company], " & _
            "[permit_number]) VALUES (" & _
            "#" & rsFAAP_Account.Fields("Create_Date").Value & "#, " & _
            "#" & rsFAAP_Account.Fields("Permit_Due").Value & "#, " & _
            m_lngAcctID & ", " + _
            "'" & responsible_1_day_phone & "', " + _
            "'" & Other_Contact & "', " & _
            "'" + Clean_String(rsFAAP_Account_Contact.Fields("Contact")) & "', " & _
            rsFAAP_Account.Fields("Monitoring_Alarm_Company").Value & ", " & _
            rsFAAP_Account.Fields("Servicing_Alarm_Company").Value & ", " & _
            "'" & rsFAAP_Account.Fields("Number").Value & "');"
      End If

         'rsReg.Fields("registration_last").Value = rsFAAP_Account.Fields("Create_Date").Value
         
         'rsReg.Fields("registration_expire").Value = rsFAAP_Account.Fields("Permit_Due").Value
         ' rsReg.Fields("account_id").Value = counter
         
         'rsReg.Fields("responsible_1_name").Value = Other_Contact
         'rsReg.Fields("contact_1").Value = rsFAAP_Account_Contact.Fields("Contact")
         'rsReg.Fields("Monitoring_Alarm_Company").Value = rsFAAP_Account.Fields("Monitoring_Alarm_Company").Value
         'rsReg.Fields("Servicing_Alarm_Company").Value = rsFAAP_Account.Fields("Servicing_Alarm_Company").Value
         'Removed 9-29-05 Do not want registration numbers
         'rsReg.Fields("permit_number") = counter
             
         
'         MsgBox (RegCommand.CommandText)
        
         rsReg.Open RegCommand
        
         '.AddNew

         StreetSplit = Split(Clean_String(rsFAAP_Account_Contact.Fields("Address1").Value), " ")
         strSortStreet = Split(Clean_String(rsFAAP_Account_Contact.Fields("Sort_Street").Value), " ")
         If UBound(strSortStreet) < 1 Then
            ReDim strSortStreet(1)
            If UBound(StreetSplit) < 2 Then
               strSortStreet(0) = StreetSplit(1)
            Else
               strSortStreet(0) = StreetSplit(1) + " " + StreetSplit(2)
            End If
         ElseIf strSortStreet(0) = "" Then
            If UBound(StreetSplit) < 2 Then
               strSortStreet(0) = StreetSplit(1)
            Else
               strSortStreet(0) = StreetSplit(1) + " " + StreetSplit(2)
            End If
         End If
         
         '         alarmcount = CountAlarm(rsFAAP_Account.Fields("ID").Value)
        
         '.Fields("ID").Value = counter
         '.Fields("Account_ID").Value = counter
         '.Fields("Addressee").Value = rsFAAP_Account_Contact.Fields("Contact").Value
         '.Fields("Street").Value = rsFAAP_Account_Contact.Fields("Address1").Value
         '.Fields("City").Value = rsFAAP_Account_Contact.Fields("City").Value
         '.Fields("State").Value = "FL"
         '.Fields("Zip").Value = rsFAAP_Account_Contact.Fields("Postal_Code").Value
         '.Fields("False_Alarm_Count").Value = alarmcount
         '.Fields("Billingl_Addressee").Value = rsFAAP_Account_Contact.Fields("Contact").Value
         '.Fields("Billing_Street").Value = Billing_Street
         '.Fields("Billing_City").Value = Billing_City
         '.Fields("Billing_State").Value = Billing_State
         '.Fields("Billing_Zip").Value = Billing_Zip
         '.Fields("Start_Date").Value = rsFAAP_Account.Fields("Create_Date").Value
         '.Fields("StreetName").Value = rsFAAP_Account_Contact.Fields("Sort_Street").Value
         '.Fields("StreetNumber").Value = StreetSplit(0)
'        .Fields("Suite").Value = rsFAAP_Account_Contact.Fields("Address2").Value
 '       .Fields("Billing_Suite").Value = rsFAAP_Account_Contact.Fields("Address2").Value
        
         If (rsFAAP_Account.Fields("Residential") = True) Then
            '.Fields("PropertyType").Value = 1
            property_type = 1
         Else
            '.Fields("PropertyType").Value = 2
            property_type = 2
         End If
     
         FABCommand.CommandText = "INSERT INTO FAB_Address (" + _
            "[ID]," + _
            "[Account_ID]," + _
            "[Addressee]," + _
            "[Street]," + "[City]," & "[State]," + "[Zip]," + _
            "[False_Alarm_Count]," + _
            "[Billingl_Addressee]," + _
            "[Billing_Street]," + "[Billing_City]," + "[Billing_State]," & "[Billing_Zip]," + _
            "[Start_Date]," + _
            "[StreetName]," + _
            "[StreetNumber]," + _
            "[PropertyType]) VALUES(" & _
            counter & ", " & _
            m_lngAcctID & ", " & _
            "'" + Clean_String(strAddressee) & "', " + _
            "'" & Clean_String(rsFAAP_Account_Contact.Fields("Address1").Value) & "', " & _
            "'" + Clean_String(rsFAAP_Account_Contact.Fields("City").Value) & "', " + _
            "'" + Clean_String(rsFAAP_Account_Contact.Fields("Region_Code").Value) + "', " & _
            "'" + rsFAAP_Account_Contact.Fields("Postal_Code").Value & "', " & _
            0 & ", " + _
            "'" & Clean_String(rsFAAP_Account_Contact.Fields("Contact").Value) & "', " + _
            "'" & Billing_Street & "', '" & Billing_City & "', '" & Billing_State & "', '" & Billing_Zip & "', " + _
            "#" & rsFAAP_Account.Fields("Create_Date").Value & "#, " + _
            "'" & Clean_String(strSortStreet(0)) & "', " + _
            "'" & StreetSplit(0) & "', " & property_type & ");"
'         MsgBox (FABCommand.CommandText)
         FABRecordSet.Open FABCommand
        
         Call ImportIncidents(rsFAAP_Account.Fields("ID"), counter)
         rsFAAP_Account.MoveNext
      Loop ' While not rsFAAP_Account.Eof
   
      'FABRecordSet.Update
      'rsReg.Update
      'Close it all
      'rsReg.Close
      
      rsFAAP_Account.Close
      MsgBox ("Finished")
      
      'FABRecordSet.Close

End Sub

' import alarm company
Private Sub Command2_Click()

   Dim CollierConnectionStr
   Dim FABConnectionStr
   Dim CollierConnection As New ADODB.Connection
   Dim CollierCommand As New ADODB.Command
   Dim CollierRecordSet As New ADODB.Recordset
   Dim CollierConnection2 As New ADODB.Connection
   Dim CollierCommand2 As New ADODB.Command
   Dim CollierRecordSet2 As New ADODB.Recordset
   Dim FABConnection As New ADODB.Connection
   Dim FABCommand As New ADODB.Command
   Dim FABRecordSet As New ADODB.Recordset
   Dim counter As Integer
   Dim StreetSplit
   Dim tempsql As String
   Dim alarmcount
  
   counter = 0

   'All the database opening stuff
   CollierConnectionStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\faap.mdb;Mode=ReadWrite|Share Deny None;Persist Security Info=False"
   FABConnectionStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\False_Alarm_Billing.mdb;Mode=ReadWrite|Share Deny None;Persist Security Info=False"
   
   CollierConnection.ConnectionString = CollierConnectionStr
   CollierConnection2.ConnectionString = CollierConnectionStr
   FABConnection.ConnectionString = FABConnectionStr
      
   CollierConnection.CursorLocation = adUseClient
   CollierConnection2.CursorLocation = adUseClient
   FABConnection.CursorLocation = adUseClient
   
   CollierConnection.Open
   CollierConnection2.Open
   FABConnection.Open
   
   'Set the data I want to pull and the table ill be using.
   With CollierCommand
      .ActiveConnection = CollierConnection
      .CommandText = "SELECT * FROM Alarm_Company;"
      .CommandType = adCmdText
   End With
   
   With FABCommand
      .ActiveConnection = FABConnection
      .CommandText = "SELECT * FROM FAB_Alarm_Company;"
      .CommandType = adCmdText
   End With

   'Open my SQL queries
   Label1.Caption = "Opening Account Table Query"
   Me.Refresh
   With CollierRecordSet
      .CursorType = adOpenStatic
      .CursorLocation = adUseClient
      .LockType = adLockOptimistic
      .Open CollierCommand
   End With
   Label1.Caption = "Opening FAB Query"
   Me.Refresh
   With FABRecordSet
      .CursorType = adOpenStatic
      .CursorLocation = adUseClient
      .LockType = adLockOptimistic
      .Open FABCommand
   End With
   
   With CollierCommand2
      .ActiveConnection = CollierConnection2
      .CommandType = adCmdText
   End With
        
   With CollierRecordSet2
      .CursorType = adOpenForwardOnly
      .CursorLocation = adUseClient
      .LockType = adLockReadOnly
   End With
   
   'Start at first record,  go through all of them also pulling the Primary contact info from a seprate DB
   CollierRecordSet.MoveFirst
   ProgressBar1.Max = CollierRecordSet.RecordCount
   
   Do While (Not CollierRecordSet.EOF)
      counter = counter + 1
      ProgressBar1.Value = counter
      Label1.Caption = "Importing Record " & counter & " of " & CollierRecordSet.RecordCount
      Me.Refresh
    
      'Find the Matching Primary Contact
      tempsql = "SELECT * FROM Alarm_Company_Contact WHERE Alarm_Company =" & CollierRecordSet.Fields("Id") & ";"
      CollierCommand2.CommandText = tempsql
      CollierRecordSet2.Open CollierCommand2
      'End Find Primary Contact

      If Not CollierRecordSet2.EOF And CollierRecordSet.Fields("Id") <> 6533 And CollierRecordSet.Fields("Id") <> 6534 Then
         With FABRecordSet
            .AddNew
            .Fields("ID").Value = CollierRecordSet.Fields("ID").Value
            .Fields("Name").Value = CollierRecordSet.Fields("Name").Value
            .Fields("Addr1").Value = CollierRecordSet2.Fields("Address1").Value
            .Fields("Addr2").Value = Clean_String(CollierRecordSet2.Fields("Address2").Value)
            .Fields("State").Value = CollierRecordSet2.Fields("Region_Code").Value
            .Fields("Zip").Value = CollierRecordSet2.Fields("Postal_Code").Value
            .Fields("Phone").Value = CollierRecordSet2.Fields("Phone_Number").Value
            .Fields("Contact").Value = CollierRecordSet2.Fields("Contact").Value
            .Fields("City").Value = CollierRecordSet2.Fields("City").Value
            .Fields("Monitoring").Value = IIf(CollierRecordSet.Fields("Monitoring").Value, 1, 0)
            .Fields("Servicing").Value = IIf(CollierRecordSet.Fields("Servicing").Value, 1, 0)
            .Update
         End With
      End If

      CollierRecordSet.MoveNext
      CollierRecordSet2.Close
   Loop ' While not CollierRecordSet.Eof

   'Close it all
   
   CollierRecordSet.Close
   FABRecordSet.Close
   
   Label1.Caption = "Finished!!!,  Imported " & counter & " Records Hooraay!!!!!"

End Sub

Private Function Clean_String(to_clean)
        
   to_clean = Trim(to_clean)
   to_clean = Replace(to_clean, ",", "")
   to_clean = Replace(to_clean, ":", "")
   to_clean = Replace(to_clean, Chr(34), "")
   to_clean = Replace(to_clean, "~", "")
   to_clean = Replace(to_clean, "|", "")
   to_clean = Replace(to_clean, "\", "")
   to_clean = Replace(to_clean, "#", "")
   to_clean = Replace(to_clean, ";", "")
   to_clean = Replace(to_clean, "*", "")
   to_clean = Replace(to_clean, "'", "`")
   to_clean = Replace(to_clean, "&", "")
   to_clean = Replace(to_clean, "+", "")
   to_clean = Replace(to_clean, "/", "")
   to_clean = Replace(to_clean, ".", "")
   Clean_String = to_clean

End Function

Private Function ImportIncidents(ByVal Account_ID As Long, ByVal Generated_ID As Long)

   Dim CollierConnectionStr
   Dim FABConnectionStr
   Dim CollierConnection As New ADODB.Connection
   Dim CollierCommand As New ADODB.Command
   Dim CollierRecordSet As New ADODB.Recordset
   
   Dim PastAlarmsConnection As New ADODB.Connection
   Dim PastAlarmsRecordSet As New ADODB.Recordset
   Dim PastAlarmsCommand As New ADODB.Command
   
   Dim CollierConnection2 As New ADODB.Connection
   Dim CollierCommand2 As New ADODB.Command
   Dim CollierRecordSet2 As New ADODB.Recordset
   
   Dim FABConnection As New ADODB.Connection
   Dim FABCommand As New ADODB.Command
   Dim FABRecordSet As New ADODB.Recordset
   Dim counter As Integer
   Dim StreetSplit
   Dim tempsql As String
   Dim alarmcount
   Dim dtIncident As Date
   Dim dtStart As Date
   Dim notes As String
   Dim Journal_Type As Integer
   Dim FAB_Incident_ID As String
   Dim Ordinal As Integer
   Dim Past_Alarm_Count As Integer
  
   counter = 1

   'All the database opening stuff
   CollierConnectionStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\faap.mdb;Mode=ReadWrite|Share Deny None;Persist Security Info=False"
   FABConnectionStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\False_Alarm_Billing.mdb;Mode=ReadWrite|Share Deny None;Persist Security Info=False"
   
   CollierConnection.ConnectionString = CollierConnectionStr
   CollierConnection2.ConnectionString = CollierConnectionStr
   FABConnection.ConnectionString = FABConnectionStr
   PastAlarmsConnection.ConnectionString = FABConnectionStr


   CollierConnection2.CursorLocation = adUseClient
   CollierConnection.CursorLocation = adUseClient
   FABConnection.CursorLocation = adUseClient
   PastAlarmsConnection.CursorLocation = adUseClient
   
   CollierConnection.Open
   FABConnection.Open
   CollierConnection2.Open
   PastAlarmsConnection.Open
   
   'Set the data I want to pull and the table ill be using.
   With CollierCommand
      .ActiveConnection = CollierConnection
      .CommandText = "SELECT * FROM Alarm WHERE Account=" & Account_ID & " ORDER BY Date ASC, Time Asc;"
      .CommandType = adCmdText
   End With
   
   With CollierCommand2
      .ActiveConnection = FABConnection
      .CommandText = "SELECT * FROM FAB_Journal;"
      .CommandType = adCmdText
   End With
   
   With FABCommand
      .ActiveConnection = FABConnection
      .CommandText = "SELECT * FROM FAB_Incident;"
      .CommandType = adCmdText
   End With
   
   With PastAlarmsCommand
      .ActiveConnection = FABConnection
      .CommandType = adCmdText
   End With
   
   'Open my SQL queries
   
   Me.Refresh
   With CollierRecordSet
      .CursorType = adOpenForwardOnly
      .CursorLocation = adUseClient
      .LockType = adLockReadOnly
      .Open CollierCommand
   End With

   Me.Refresh
   With FABRecordSet
      .CursorType = adOpenForwardOnly
      .CursorLocation = adUseClient
      .LockType = adLockReadOnly
      '.Open FABCommand
   End With
   
   With CollierRecordSet2
      .CursorType = adOpenForwardOnly
      .CursorLocation = adUseClient
      .LockType = adLockReadOnly
   End With
   
   With PastAlarmsRecordSet
      .CursorType = adOpenForwardOnly
      .CursorLocation = adUseClient
      .LockType = adLockReadOnly
   End With
   
   'Start at first record,  go through all of them also pulling the Primary contact info from a seprate DB
   
   If Not (CollierRecordSet.EOF) Then
      CollierRecordSet.MoveFirst
      ProgressBar2.Max = CollierRecordSet.RecordCount
      
      Do While (Not CollierRecordSet.EOF)
         ProgressBar2.Value = counter - 1
         counter = counter + 1
         Label2.Caption = "Importing Incident " & counter - 1 & " of " & CollierRecordSet.RecordCount
         Me.Refresh
         DoEvents
         'If CollierRecordSet.Fields("Date").Value > twoDate Then
         With FABRecordSet
            '.AddNew
            'RegRecordSet.AddNew
            'RegRecordSet.Fields("registration_expire").Value = CollierRecordSet.Fields("Permit_Due")
            'RegRecordSet.Fields("registration_initial").Value = CollierRecordSet.Fields("Create_Date")
            'RegRecordSet.Fields("account_id").Value = Generated_ID
            'RegRecordSet.Update
            If CollierRecordSet.Fields("Number").Value = "" Then
               FAB_Incident_ID = "ID" & Account_ID & "-" & counter
            Else
               FAB_Incident_ID = CollierRecordSet.Fields("Number").Value
            End If
            '.Fields("External_ID").Value = "ID" & Generated_ID & "-" & counter
            '.Fields("Address_ID").Value = Generated_ID
            dtIncident = Format$(CollierRecordSet.Fields("Date").Value, "mm/dd/yyyy") & " " & Format$(CollierRecordSet.Fields("Time"), "hh:mm AM/PM")
            dtStart = "01/01/" + Format$(dtIncident, "yyyy")
            
            PastAlarmsCommand.CommandText = "SELECT External_ID, Date_Time FROM FAB_Incident " & _
               "WHERE Account_ID=" & CStr(Account_ID) & _
               " AND Date_Time >= #" & Format$(dtStart, "mm/dd/yyyy") & "# " + _
               "AND Ordinal <> 26 " + _
               "ORDER BY date_Time;"
'            Clipboard.SetText (PastAlarmsCommand.CommandText)
            PastAlarmsRecordSet.Open PastAlarmsCommand
            Past_Alarm_Count = PastAlarmsRecordSet.RecordCount
            If Past_Alarm_Count = 0 Then
'               MsgBox (PastAlarmsCommand.CommandText)
            End If
            PastAlarmsRecordSet.Close
            
            '.Fields("Date_Time").Value = dt
            '.Fields("Batch_ID").Value = 0
            '.Fields("Account_ID").Value = Generated_ID
            '.Fields("Disposition").Value = CollierRecordSet.Fields("Disposition")
            
            If CollierRecordSet.Fields("Disposition").Value = 8 Or _
                  CollierRecordSet.Fields("Disposition").Value = 21 Or _
                  CollierRecordSet.Fields("Disposition").Value = 22 Or _
                  CollierRecordSet.Fields("Disposition").Value = 23 Then
               Ordinal = 26
            Else
               If Int(Past_Alarm_Count) < 11 Then
                  '.Fields("Ordinal").Value = counter
                  Ordinal = Past_Alarm_Count + 1
               Else
                  '.Fields("Ordinal").Value = 11
                  Ordinal = 11
               End If
            End If
            
            '.Fields("Appeal_Status").Value = 0
            '.Fields("Type").Value = "F"
            
            notes = Clean_String(CollierRecordSet.Fields("Comments"))
            
            '.Fields("Notes") = notes
            
            On Error GoTo IncidentErr
            FABCommand.CommandText = "INSERT INTO FAB_Incident ([External_ID], [Address_ID], [Date_Time]," & _
               "[Batch_ID], [Account_ID], [Disposition], [Ordinal], [Appeal_Status], [Type], [Notes])" & _
               " VALUES ('" & _
                              FAB_Incident_ID & "', " & _
                              Account_ID & _
                              ", #" & dtIncident & _
                              "#, 0, " & _
                              Account_ID & ", " & _
                              CollierRecordSet.Fields("Disposition").Value & ", " & _
                              Ordinal & _
                              ", 0, 'P', '" & _
                              notes & "');"
         '   MsgBox (FABCommand.CommandText)
            FABRecordSet.Open FABCommand
            
            On Error GoTo 0
            
         End With
          
         With CollierRecordSet2
            '.AddNew
            '.Fields("Account_ID").Value = Generated_ID
            '.Fields("Disposition").Value = CollierRecordSet.Fields("Disposition")
            '.Fields("Date_Time") = dt
            '.Fields("Amount") = "0.00"
            '.Fields("Incident_ID") = "ID" & Generated_ID & "-" & counter
            '.Fields("Notes") = Clean_String(CollierRecordSet.Fields("Comments"))
            '.Fields("Entry_Date_Time") = dt
            
            If Ordinal = 26 Then
               Journal_Type = Ordinal
            Else
               Journal_Type = Ordinal + 2
            End If
            
            CollierCommand2.CommandText = "INSERT INTO FAB_Journal ([Account_ID], [Journal_Type_ID]," & _
               " [Disposition], [Date_Time], [Amount], [Incident_ID], [Notes], [Entry_Date_Time]) VALUES " & _
               "(" & Account_ID & ", " & Journal_Type & ", " & Int(CollierRecordSet.Fields("Disposition")) & _
               ", #" & dtIncident & "#, '0.00', '" & FAB_Incident_ID & "', '" & Clean_String(CollierRecordSet.Fields("Comments")) & _
               "', #" & Format$(Now, "mm/dd/yyyy hh:mm am/pm") & "#);"
            'MsgBox (CollierCommand2.CommandText)
            CollierRecordSet2.Open CollierCommand2
            '        CollierRecordSet2.Close
         End With
         'End If
         CollierRecordSet.MoveNext
      Loop ' While not CollierRecordSet.Eof
   End If

   'Close it all
   'FABRecordSet.Update
   'CollierRecordSet2.Update
   'FABRecordSet.Close
   'CollierRecordSet2.Close
   CollierRecordSet.Close
   Exit Function
   
IncidentErr:

   If Err.Number = -2147467259 Then
      On Error GoTo 0
      FAB_Incident_ID = FAB_Incident_ID + "-" & CStr(Account_ID) + "-" + CStr(counter)
      FABCommand.CommandText = "INSERT INTO FAB_Incident ([External_ID], [Address_ID], [Date_Time]," & _
         "[Batch_ID], [Account_ID], [Disposition], [Ordinal], [Appeal_Status], [Type], [Notes])" & _
         " VALUES ('" & _
                        FAB_Incident_ID & "', " & _
                        Account_ID & _
                        ", #" & dtIncident & _
                        "#, 0, " & _
                        Account_ID & ", " & _
                        CollierRecordSet.Fields("Disposition") & ", " & _
                        Ordinal & _
                        ", 0, 'P', '" & _
                        notes & "');"
                        
   '   MsgBox (FABCommand.CommandText)
   '   Debug.Print FABCommand.CommandText
      
      FABRecordSet.Open FABCommand
      Resume Next
   Else
      MsgBox Err.Description
   End If
   
End Function

'The Destroy Dupes
Private Sub Command3_Click()

   Dim FABConnectionStr
   Dim DupeConnection As New ADODB.Connection
   Dim DupeCommand As New ADODB.Command
   Dim DupeRecordSet As New ADODB.Recordset
   Dim CollierConnection As New ADODB.Connection
   Dim CollierCommand As New ADODB.Command
   Dim CollierRecordSet As New ADODB.Recordset
   Dim counter
   Dim counter2
   Dim firstfour
   Dim DupeCount
  
   counter = 0
   DupeCount = 0
   FABConnectionStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\False_Alarm_Billing.mdb;Mode=ReadWrite|Share Deny None;Persist Security Info=False"
   CollierConnection.ConnectionString = FABConnectionStr
   CollierConnection.CursorLocation = adUseClient
   CollierConnection.Open
   DupeConnection.ConnectionString = FABConnectionStr
   DupeConnection.CursorLocation = adUseClient
   DupeConnection.Open
   'Set the Recordset Parameters
   
   With CollierCommand
      .ActiveConnection = CollierConnection
      .CommandText = "SELECT ID, Street, Suite FROM FAB_Address;"
      .CommandType = adCmdText
   End With
   
   With CollierRecordSet
      .CursorType = adOpenStatic
      .CursorLocation = adUseClient
      .LockType = adLockOptimistic
      .Open CollierCommand
   End With
   
   DupeCommand.ActiveConnection = CollierConnection
   DupeCommand.CommandType = adCmdText
   DupeRecordSet.CursorType = adOpenStatic
   DupeRecordSet.CursorLocation = adUseClient
   DupeRecordSet.LockType = adLockOptimistic
   CollierRecordSet.MoveFirst
   ProgressBar1.Max = CollierRecordSet.RecordCount
   Do While (Not CollierRecordSet.EOF)
      counter = counter + 1
      ProgressBar1.Value = counter
      Label1.Caption = "Checking For Dupes on Account " & counter
      DupeCommand.CommandText = "SELECT ID, Addressee, Street, Suite FROM FAB_Address WHERE ((Street = """ & CollierRecordSet.Fields("Street") & """) AND (ID <> " & CollierRecordSet.Fields("ID").Value & "));"
      
      DupeRecordSet.Open DupeCommand
      counter2 = 0
      If Not DupeRecordSet.EOF Then
      
      If Left$(DupeRecordSet("Street").Value, 3) = "633" Then
      Debug.Print DupeCommand.CommandText
      End If
      
          DupeRecordSet.MoveFirst
      End If
      Do While Not (DupeRecordSet.EOF)
          counter2 = counter2 + 1
          Label2.Caption = "Fixing Dupes for account " & counter & "!"
          firstfour = Split(DupeRecordSet.Fields("addressee").Value, " ")
          If (DupeRecordSet.Fields("suite").Value <> "") Then
              DupeRecordSet.Fields("suite").Value = "DUP" & counter2
              DupeRecordSet.Update
              DupeCount = DupeCount + 1
              Label3.Caption = DupeCount & " Total Dupes Fixed"
          End If
          DupeRecordSet.MoveNext
          DoEvents
      Loop
      DupeRecordSet.Close
      CollierRecordSet.MoveNext
   Loop
    
   CollierRecordSet.Close

End Sub

